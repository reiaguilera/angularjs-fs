module.exports = function Proyectos(util, Usuarios, ObjectId) {
    var coleccion = "proyectos";
    var async = require('async');

    function getProyectos(usuario, cb) {
        util.test("Buscando proyectos para :" + JSON.stringify(usuario));
        util.conectarMongo(coleccion, function (err, collection) {
            collection.find({
                $or: [{
                    manager: usuario.email
                }, {
                    cliente: usuario.email
                }]
            }).toArray(cb);
        })
    }

    function getEmailClientes(filtro, cb) {
        util.test("Buscando email clientes");
        util.conectarMongo(coleccion, function (err, collection) {
            collection.find({
                cliente: RegExp(filtro)
            }, {
                'cliente': true
            }).toArray(cb);
        })
    }

    function getProyectosPagina(usuario, pagina, cb) {
        util.test("Buscando proyectos pagina : " + pagina);
        var q = {
            $or: [{
                manager: usuario.email
                }, {
                cliente: usuario.email
                }]
        };
        var camposManager = {
            manager: 0
        }; // Ya se sabe quien es
        var camposCliente = {
            cliente: 0,
            coste: 0
        }; // El cliente no debe saber el coste

        async.waterfall([
            function (callback) {
                util.conectarMongo(coleccion, callback);
            },
            function (collection, callback) {
                collection.find(q).skip(3 * (pagina - 1)).limit(3).toArray(callback);
            }
        ], cb);


        //        util.conectarMongo(coleccion, function (err, collection) {
        //            collection.find(q).skip(3 * (pagina - 1)).limit(3).toArray(cb);
        //        })
    }


    function getProyectosFiltro(filtro, usuario, cb) {
        util.test("Buscando proyectos para :" + JSON.stringify(usuario) + " que contengan: " + filtro);
        util.conectarMongo(coleccion, function (err, collection) {
            collection.find({
                //                $or: [{
                //                    manager: usuario.email
                //                }, {
                //                    cliente: usuario.email
                //                }],
                nombre: RegExp(filtro)
            })
                .toArray(cb);
        })
    }

    function getProyecto(_id, usuario, cb) {
        var q = {
            _id: new ObjectId(_id),
            $or: [{
                manager: usuario.email
            }, {
                cliente: usuario.email
            }]
        };
        util.conectarMongo(coleccion, function (err, collection) {
            collection.findOne(q, cb);
        })
    }

    function getNumProyectos(usuario, cb) {
        var q = {
            $or: [{
                manager: usuario.email
            }, {
                cliente: usuario.email
            }]
        };
        util.conectarMongo(coleccion, function (err, collection) {
            collection.count(q, cb);
        })
    }

    function postProyecto(usuario, doc, cb) {
        if (!doc.manager) doc.manager = usuario.email;
        if (doc.manager === usuario.email) {
            util.test("Insertando el proyecto " + JSON.stringify(doc));
            util.conectarMongo(coleccion, function (err, collection) {
                collection.findOne({
                    manager: usuario.email,
                    nombre: doc.nombre
                }, function (err, docDB) {
                    if (err) {
                        cb(err, null);
                    } else {
                        if (docDB) {
                            util.test("Ya existe otro proyecto con ese nombre:  " + JSON.stringify(doc));
                            cb(new Error("Ya existe otro proyecto con ese nombre"), null);
                        } else {
                            collection.insert(doc, function (err, docsDB) {
                                if (err) {
                                    cb(err, null);
                                } else {
                                    var docDB = docsDB[0];
                                    util.test("Insertado proyecto:  " + JSON.stringify(docDB));
                                    var cliente = {
                                        nombre: doc.cliente,
                                        email: doc.cliente,
                                        password: doc.cliente,
                                        rol: "cliente"
                                    };
                                    var usuarios = new Usuarios(util);
                                    usuarios.postUsuario(cliente, function (err, usuarioDB) {
                                        if (err) {
                                            if (err.toString().indexOf("El Usuario ya existe") >= 0) {
                                                util.test("El cliente ya existe el proyecto va bien:  " + JSON.stringify(usuarioDB));
                                                cb(null, docDB);
                                            } else {
                                                util.test("Eliminando proyecto por no poder guardar:  " + JSON.stringify(cliente));
                                                collection.remove({
                                                    id: docDB._id
                                                }, function (err, result) {
                                                    if (err) {
                                                        cb(err, null);
                                                    } else {
                                                        util.test("No se ha guardado el proyecto pues no se guardó el cliente:  " + JSON.stringify(cliente));
                                                        cb(new Error("No se ha guardado el proyecto pues no se guardó el cliente"), null);
                                                    }
                                                })
                                                cb(err, null);
                                            }
                                        } else {
                                            util.test("Insertado cliente:  " + JSON.stringify(usuarioDB));
                                            cb(null, docDB);
                                        }
                                    })
                                }
                            });
                        }
                    }
                })
            })
        } else {
            util.test("Sin manager o incorrecto:  " + JSON.stringify(doc));
            cb(new Error("Sin manager o incorrecto"), null);
        }
    }

    function putProyecto(usuario, doc, cb) {
        if (doc.manager === usuario.email || doc.cliente === usuario.email) {
            util.test("Guardando el proyecto " + JSON.stringify(doc));
            util.conectarMongo(coleccion, function (err, collection) {
                getProyecto(doc._id, usuario, function (err, docDB) {
                    docDB.estado = doc.estado;
                    docDB.coste = doc.coste;
                    docDB.rating = doc.rating;
                    collection.save(docDB, cb);
                });

            })
        } else {
            util.test("Sin permisos o incorrecto:  " + JSON.stringify(doc));
            cb(new Error("Sin permisos o incorrecto"), null);
        }
    }




    return {
        getProyectos: getProyectos,
        getNumProyectos: getNumProyectos,
        getProyectosPagina: getProyectosPagina,
        getProyectosFiltro: getProyectosFiltro,
        getEmailClientes: getEmailClientes,
        getProyecto: getProyecto,
        postProyecto: postProyecto,
        putProyecto: putProyecto
    };
};
(function () {
    var UsuariosCtrl = function ($rootScope, $location, $cookieStore, usuariosFactory, sesionesFactory, menuFactory) {
        var vm = this;
        vm.usuario = {};
        vm.sesion = {};

        vm.test = "jasmine";
        
        vm.registro = function () {
            if (vm.usuario.email.indexOf("gmail") > 0) {
                vm.emailInvalido = true;
            } else {
                vm.emailInvalido = false;
                usuariosFactory.postUsuario(vm.usuario)
                    .success(function (usuario) {
                        if (usuario) {
                            $rootScope.nombre = vm.usuario.email;
                            $rootScope.mensaje = 'recién creado';
                            $cookieStore.put("idSesion", usuario.idSesion);
                            $cookieStore.put("usuario" , usuario);
                            $rootScope.opcionesMenu = menuFactory.query();
                            $location.path("/");
                        } else {
                            $rootScope.mensaje = 'Algo ha salido mal';
                        }
                    }).error(function (err) {
                        console.log("error al registrar nuevo usuario: " + err);
                    });
            }
        };

        vm.login = function () {
            sesionesFactory.postUsuario(vm.usuario)
                    .success(function (usuario) {
                        if (usuario) {
                            $rootScope.nombre = vm.usuario.email;
                            $rootScope.mensaje = 'ha vuelto';
                            $cookieStore.put("idSesion", usuario.idSesion);
                            $cookieStore.put("usuario" , usuario);
                            $rootScope.opcionesMenu = menuFactory.query();
                            $location.path("/");
                        } else {
                            $rootScope.mensaje = 'Algo ha salido mal';
                        }
                    }).error(function (err) {
                        console.log("error al identificar a un usuario: " + err);
                    });
        }
    }

    angular
        .module("appTuCuenta")
        .controller('UsuariosCtrl', UsuariosCtrl);
}());
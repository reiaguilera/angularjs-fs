var express = require('express');
var router = express.Router();
var Proyectos = require("./model/proyectos");
var Usuarios = require("./model/usuarios");
var util = require("./util");


var proyectos = new Proyectos();
var usuarios = new Usuarios();



router.route('/api/priv/proyectos')
    .get(function (req, res, next) {
        proyectos.getProyectos(function (err, docs) {
            if (err) util.tratarError(err, res);
            res.json(docs);
        });
    })
    .post(function (req, res, next) {
        var doc = req.body;
        proyectos.postProyecto(doc, function (err, docs) {
            if (err) util.tratarError(err, res);
            res.status(200).json(doc);
        });
    });
// Registro
router.route('/api/usuarios')
    .post(function (req, res, next) {
        var doc = req.body;
        usuarios.postUsuario(doc, function (err, docs) {
            if (err) util.tratarError(err, res);
            res.status(200).json(doc);
        });
    });
// Login
router.route('/api/sesiones')
    .post(function (req, res, next) {
        var doc = req.body;
        usuarios.postSesion(doc, function (err, docs) {
            if (err) util.tratarError(err, res);
            res.status(200).json(doc);
        });
    });

module.exports.router = router;
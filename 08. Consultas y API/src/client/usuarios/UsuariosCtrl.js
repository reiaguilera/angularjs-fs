(function () {
    var UsuariosCtrl = function ($rootScope, $location, $cookieStore, usuariosFactory, sesionesFactory) {
        var vm = this;
        vm.usuario = {};
        vm.sesion = {};

        vm.registro = function () {
            if (vm.usuario.email.indexOf("gmail") > 0) {
                vm.emailInvalido = true;
            } else {
                vm.emailInvalido = false;
                usuariosFactory.postUsuario(vm.usuario)
                    .success(function (res) {
                        var idSesion = res;
                        if (idSesion) {
                            console.log("idSesion recibida: " + idSesion);
                            $rootScope.nombre = vm.usuario.email;
                            $rootScope.mensaje = 'recién creado';
                            $cookieStore.put("idSesion", idSesion);
                            $location.path("/");
                        } else {
                            $rootScope.mensaje = 'Algo ha salido mal';
                        }
                    }).error(function (err) {
                        console.log("error al recibir idSesion: " + err);
                    });
            }
        };

        vm.login = function () {
            sesionesFactory.postUsuario(vm.usuario)
                    .success(function (res) {
                        var idSesion = res;
                        if (idSesion) {
                            console.log("idSesion recibida: " + idSesion);
                            $rootScope.nombre = vm.usuario.email;
                            $rootScope.mensaje = 'ha vuelto';
                            $cookieStore.put("idSesion", idSesion);
                            $location.path("/");
                        } else {
                            $rootScope.mensaje = 'Algo ha salido mal';
                        }
                    }).error(function (err) {
                        console.log("error al recibir idSesion: " + err);
                    });
        }
    }

    angular
        .module("appTuCuenta")
        .controller('UsuariosCtrl', UsuariosCtrl);
}());
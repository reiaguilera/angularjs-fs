(function () {
    var proyectosFactory =   function ($resource)  {
        return $resource("/api/priv/proyectos/", {});
    };

    angular.module("appTuCuenta").factory('proyectosFactory',proyectosFactory);
}());
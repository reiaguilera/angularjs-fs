describe('Probando el controlador de usuarios', function () {
    var $rootScope, $location, $cookieStore, usuariosFactory, sesionesFactory, menuFactory;

    beforeEach(function () {

        module('appTuCuenta');

        inject(function ($rootScope, $controller, $injector) {
            $scope = $rootScope.$new();
            $location = $injector.get('$location');

            $rootScope = $rootScope;
            $location = $injector.get('$location');
            $cookieStore = $injector.get('$cookieStore');
            usuariosFactory = $injector.get('usuariosFactory');
            sesionesFactory = $injector.get('sesionesFactory');
            menuFactory = $injector.get('menuFactory');


            ctrl = $controller('UsuariosCtrl as vm', {
                $scope: $scope
            });
        });
    });

    it('arranca bien el controlador', function () {
        expect($scope.vm.test).toEqual("jasmine");
    });

});
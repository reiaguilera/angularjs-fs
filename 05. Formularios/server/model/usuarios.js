var util = require("../util");
module.exports = function Usuarios() {
    var coleccion = "usuarios";

    return {
        // registro
        postUsuario: function (usuario, cb) {
            util.conectarMongo(coleccion, function (err, usuarios) {
                if (err) cb(err, null);
                // To Do: comprobar si el usuario ya existe
                usuario.idSesion = 
                    Math.floor(Math.random() * (88888) + 11111);
                usuario.timeStamp = new Date();
                // persistirlo en mongo
                usuarios.insert(usuario, cb);
            })
        },
        // inicio de sesión
        postSesion: function (doc, cb) {
            util.conectarMongo(coleccion, function (err, collection) {
                // Encontrar usuario
                collection.findOne({
                        email: doc.email,
                        password: doc.password
                    },
                    function (err, docDB) {
                        if (err) cb(err, null);
                        // Crear sesion 
                        docDB.idSesion = Math.floor(Math.random() * (88888) + 11111);
                        docDB.timeStamp = new Date();
                        // Actualizar base de datos
                        collection.save({
                            _id: docDB.id
                        }, cb);
                    })
            })
        },
        getSesion: function (idSesionBuscada, res, next, cb) {
            util.conectarMongo(coleccion, function (err, collection) {
                // Encontrar usuario
                util.test('buscamos el usuario con la sesion: ' + idSesionBuscada);
                collection.findOne({
                    idSesion: parseInt(idSesionBuscada)
                }, function(err, usuario){
                    if(err) cb(err, res, next, null);
                    cb(null, res, next, usuario);
                });

            })

        }
    };
};
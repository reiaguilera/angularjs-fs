module.exports.test = function (value) {
    console.log("Desde nodemon: " + value);
}

module.exports.tratarError = function (err, res) {
    if (err) console.log(err);
    if (res) res.status(500).send(err);
}


var config = require("./config");
var configuracion = config.configuracion();
var mongoDB = configuracion.mongoDB;
var MongoClient = require('mongodb').MongoClient;
module.exports.conectarMongo =
    function connect(coleccion, callBackFunction) {
        MongoClient.connect(mongoDB, function (err, db) {
            if (err) {
                callBackFunction(err, null);
            }
            var collection = db.collection(coleccion);
            callBackFunction(null, collection);
        })
};


module.exports.controlDeSesion = function (err, res, next, usuario) {
    if (err) res.status(500).send(err);
    if (usuario) {
        if ((new Date() - usuario.timeStamp) > 20 * 60 * 1000) {
            console.log(
                'Sesión caducada:' + JSON.stringify(usuario));
            res.status(419).send('Sesión caducada');
        } else {
            usuario.timeStamp = new Date();
//            MongoClient.connect(mongoDB, function (err, db) {
//                db.collection("usuarios").save(usuario, function () {
//                    console.log('usuario con sesión ampliada ' + JSON.stringify(usuario));
//                    res.usuario = usuario;
//                    next();
//                });
//            })
            res.usuario = usuario;
            next();
        }
    } else {
        console.log('usuario no encontrado Credencial inválida :-(');
        res.status(401).send('Credencial inválida');
    }
}
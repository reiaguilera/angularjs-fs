(function () {
    var proyectosFactory =   function ($resource)  {
        return $resource("/api/priv/proyectos/:_id", {}, {
            'update': {
                method: 'PUT'
            }
        });
    };

    angular.module("appTuCuenta").factory('proyectosFactory', proyectosFactory);
}());
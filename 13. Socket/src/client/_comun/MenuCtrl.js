(function () {
    var MenuCtrl = function ($location, $rootScope, $cookieStore, menuFactory, socketFactory) {
        var vm = this;
        vm.isActive = function (ruta) {
            return ruta === $location.path();
        }

        vm.logOut = function () {
            console.log("saliendo");
            $cookieStore.remove("idSesion");
            $cookieStore.remove("usuario");
            $location.path('/usuarios/login');
        }

        $rootScope.opcionesMenu = menuFactory.query();

        socketFactory.emit("newProj",0);
        socketFactory.on('StatusProyectos', function (data) {
            vm.msg = data.statusProyectos.length;
        });
    }
    angular
        .module('appTuCuenta')
        .controller('MenuCtrl', MenuCtrl);
}());


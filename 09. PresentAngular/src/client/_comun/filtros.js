(function () {
    angular.module('comun-Filtros', [])
        .filter('rellenar', function () {
            return function (input) {
                try {
                    if (!input || input === undefined || input.trim() === "") {
                        return '-';
                    };
                } catch (err) {
                    console.log(JSON.stringify(err));
                    return '-';
                }
            }
        })
        .filter('recortar', function () {
            return function (input, long) {
                if (!long) {
                    long = 10
                }
                if (!input || typeof input == 'undefined') {
                    return '-'
                }
                if (input.length <= long) {
                    return input
                } else {
                    return input.substring(0, long) + '...'
                }
            }
        }).filter('recortarFinal', function () {
            return function (input, long) {
                if (!long) {
                    long = 10
                }
                if (!input || typeof input == 'undefined') {
                    return '-'
                }
                if (input.length <= long) {
                    return input
                } else {
                    return '...' + input.substring(input.length - long) 
                }
            }
        });
}());
var util = require("../util");
module.exports = function Proyectos() {
    var coleccion = "proyectos";

    return {
        getProyectos: function (usuario, cb) {
            console.log(
                "Buscando proyectos para :" + JSON.stringify(usuario));                     util.conectarMongo(coleccion, function (err, collection) {
                    collection.find({manager: usuario.email}).toArray(cb);
                })
            },
        postProyecto: function (usuario, doc, cb) {
            if (doc.manager === usuario.email) {
                util.conectarMongo(coleccion, function (err, collection) {
                    // Comprobar antes si existe el proyecto
                    collection.insert(doc, cb);
                })
            }
            else{
                cb(new Error("Sin manager o incorrecto"),null);
            }
            }
        };
    };
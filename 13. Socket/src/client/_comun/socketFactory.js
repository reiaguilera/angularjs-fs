(function () {
    angular.module('appTuCuenta').factory('socketFactory', function ($rootScope) {
        var socket = io.connect('http://localhost:3000');
        return {
            on: function (eventName, callback) {
                socket.on(eventName, function () {
                    var args = arguments;
                    console.log("recibido evento: " + eventName + " datos: " + JSON.stringify(args));
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function (eventName, data) {
                socket.emit(eventName, data);
            }
        }
    });
}());
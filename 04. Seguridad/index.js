var util = require("./server/util");
var server = require('./server/routes');
var express = require('express');
var bodyParser = require('body-parser');
util.test('ready');

var Usuarios = require("./server/model/usuarios");
var usuarios = new Usuarios();

var app = express();
app.use(bodyParser.json());
app.use(express.static(__dirname + '/client'));

// Middleware de validación de sesiones
app.use('/api/priv/', function (req, res, next) {
    var idSesion = req.get('idSesion');
    var usuario = usuarios.getSesion(idSesion);
    if (usuario) {
        util.test('usuario encontrado ;-)');
        if ((new Date() - usuario.timeStamp) > 20 * 1000) {
            console.log('Sesión caducada:' + JSON.stringify(sesionEncontrada));
            res.status(419).send('Sesión caducada');
        } else {
            usuario.timeStamp = new Date();
            util.test('continuamos');
            next();
        }
    } else {
        util.test('usuario no encontrado :-(');
        res.status(401).send('Credencial inválida');
    }
    
});

app.use('/', server.router);
util.test('steady');

app.listen(3000);
util.test('go');
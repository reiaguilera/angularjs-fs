(function () {
    var proyectosFactory =   function ($resource)  {
        return $resource("/api/proyectos/", {});
    };

    angular.module("appTuCuenta").factory('proyectosFactory',proyectosFactory);
}());
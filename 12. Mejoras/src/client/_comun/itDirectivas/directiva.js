(function () {
    var rating = function () {
        return {
            restrict: 'E',
            templateUrl: './_comun/itDirectivas/estrellas.html',
            scope: {
                valor: '=',
                max: '@',
                soloLectura: '@'
            },
            link: function (scope, elem, attrs) { 
                function actualizar() {
                    scope.estrellas = [];
                    for (var i = 0; i < scope.max; i++) {
                        var estrella = {
                            marcada: (i < scope.valor)
                        };
                        scope.estrellas.push(estrella);
                    }
                };

                scope.marcar = function (indice) {
                    if (scope.soloLectura && scope.soloLectura === 'true') {
                        return;
                    }
                    scope.valor = indice + 1;
                    actualizar();
                };

                actualizar();
            }
        }
    }

    angular.module('it-directivas')
        .directive('rating', rating);

}());
var gulp = require('gulp');

var clean = require('gulp-clean');
//var changed = require('gulp-changed');
var minifyHTML = require('gulp-minify-html');
var ngAnnotate = require('gulp-ng-annotate');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var useref = require('gulp-useref');


gulp.task('limpiar', function(){
  return gulp.src(['./dist/*'], {read:false})
  .pipe(clean());
});

gulp.task('minificar-html',['limpiar'], function() {
  var htmlSrc = './src/client/**/*.html',
      htmlDst = './dist/client';
 
  gulp.src(htmlSrc)
    .pipe(minifyHTML())
    .pipe(gulp.dest(htmlDst));
});

gulp.task('minificar-js',['limpiar'], function () {
    return gulp.src('./src/client/**/*.js')
        .pipe(ngAnnotate())
        .pipe(concat("total.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/client/js'));
});

gulp.task('referencias',['minificar-js'],function () {
    var assets = useref.assets();

    return gulp.src('./src/client/index.html')
        .pipe(useref())
        .pipe(gulp.dest('./dist/client'));
});



gulp.task('copiar-server',['limpiar'], function () {
    return gulp.src([ './src/server/**/*.js'])
        .pipe(gulp.dest('./dist/server'));
});

gulp.task('copiar-index',['limpiar'], function () {
    return gulp.src([ './src/index.js','./src/package.json'])
        .pipe(gulp.dest('./dist'));
});


gulp.task('default', ['minificar-html','minificar-js', 'referencias' , 'copiar-server','copiar-index']);
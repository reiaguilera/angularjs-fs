(function () {
    var ProyectoCtrl = function ($routeParams, proyectosFactory) {
        var vm = this;
        vm.titulo = "Proyecto";
        var idProyecto = $routeParams.idProyecto;
        console.log("Buscando el proyecto: " + idProyecto);
        vm.proyecto = proyectosFactory.get({_id:idProyecto});
        
    }
    angular
        .module("appTuCuenta")
        .controller("ProyectoCtrl", ProyectoCtrl);
}());
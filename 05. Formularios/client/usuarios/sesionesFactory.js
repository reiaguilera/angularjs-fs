(function () {
    var sesionesFactory =   function ($resource)  {
        return $resource("/api/sesiones/", {});
    };

 angular
 .module("appTuCuenta")
 .factory('sesionesFactory',sesionesFactory);
}());
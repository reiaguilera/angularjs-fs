var util = require("./server/util");
var server = require('./server/routes');
var express = require('express');
var bodyParser = require('body-parser');
util.test('ready');

var app = express();
app.use(bodyParser.json());
app.use(express.static(__dirname + '/client'));
app.use('/', server.router);
util.test('steady');

app.listen(3000);
util.test('go');